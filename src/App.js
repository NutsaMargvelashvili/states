import Text from "./Inputs/TextInput";
import './App.css';
import RadioBTN from "./Inputs/RadioBTNInput";
import Range from "./Inputs/RangeInput";
import Checkbox from "./Inputs/CheckboxInput";


function App() {
  return (
    <div className="App">
        <span>First Name: </span>
        <Text/>
        <span>Last Name: </span>
        <Text/>
        <span>Sex: </span>
        <div className={"radio_BTN"}>
            <RadioBTN sex={"Male"}/>
            <RadioBTN sex={"Female"}/>
            <RadioBTN sex={"Other"}/>
        </div>
        <span>Age: </span>
        <Range/>
        <span>Choose your interests: </span>
            <Checkbox hobby={"Codding"} choosen/>
            <Checkbox hobby={"Music"}/>
            <Checkbox hobby={"Swimming"}/>
            <Checkbox hobby={"Hiking"}/>
    </div>
  );
}

export default App;
