import React, { useState } from 'react';

const RadioBTN = (props) => {
    const [getChoice, setChoice] = useState(props.sex);
    console.log(getChoice);
    const showChoice = (event) => {
        setChoice(event.target.value)
        console.log(getChoice);
    }

    return (
        <div className="radio__input">
            <input type="radio" id={props.sex} name="gender"  value={props.sex} onClick={showChoice}/>
            <label htmlFor={props.sex}>{props.sex}</label>
        </div>
    );
}

export default RadioBTN;