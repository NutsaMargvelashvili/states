import React, { useState } from 'react';

const Range = (props) => {
    const [getAge, setAge] = useState();
    console.log(getAge);
    const showAge = (event) => {
        setAge(event.target.value)
        console.log(getAge);
    }

    return (
        <div className="range__input">
            <input type="range" onChange={showAge}/>
        </div>
    );
}

export default Range;