import React, { useState } from 'react';

const Checkbox = (props) => {
    const [getHobby, setHobby] = useState(props.choosen);

    const showInterest = (event) => {
        setHobby(event.target.checked)
        console.log(getHobby);
    }

    return (
        <div className="checkbox__input">
            <input type="checkbox" id={props.hobby} name="hobby" value={props.hobby} checked={getHobby} onClick={showInterest}/>
            <label htmlFor={props.hobby}>{props.hobby}</label>
        </div>
    );
}

export default Checkbox;
