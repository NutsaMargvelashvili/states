import React, { useState } from 'react';
const Text = () => {


    const [getText, setText] = useState();
    const showText = (event) => {
        setText(event.target.value)
        console.log(getText);
    }
    return (
        <div className="text__input">
            <input type={"text"} onChange={showText}/>
        </div>
    );
}

export default Text;
